'use strict';

const { task, src, dest, series, parallel, watch } = require('gulp');
const { onError } = require('gulp-notify');
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cssnano = require('gulp-cssnano');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const gulpIf = require('gulp-if');
const concat = require('gulp-concat');
const newer = require('gulp-newer');
const plumber = require('gulp-plumber');
const gulpStylelint = require('gulp-stylelint');
const rigger = require('gulp-rigger');
const del = require('del');
const browserSync = require('browser-sync').create();

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

const path = {

  build: {
    html: 'build/',
    js: 'build/js/',
    js_libs: 'build/js/libs',
    css: 'build/css/',
    // css_libs: 'build/css/libs',
    img: 'build/assets/images/',
    fonts: 'build/assets/fonts/'
  },
  src: {
    html: 'src/*.html',
    pug: 'src/*.pug',
    js: {
      libs: 'src/js/libs/*.js',
      common: 'src/js/common/*.js'
    },
    style: {
      main: 'src/scss/main.scss',
      // libs: ['src/scss/libs/*.css', 'src/scss/libs/*.scss']
    },
    assets: {
      img: ['src/assets/images/**/*.*', '!src/assets/images/**/*.svg'],
      svg: 'src/assets/images/**/*.svg',
      fonts: 'src/assets/fonts/**/*.*'
    }
  },
  watch: {
    html: 'src/**/*.html',
    pug: 'src/**/*.pug',
    js: 'src/js/**/*.js',
    style: 'src/**/*.scss',
    img: 'src/assets/images/**/*.*',
    fonts: 'src/assets/fonts/**/*.*'
  },
  clean: './build'
};

const config = {
  server: {
    baseDir: "./build"
  },
  tunnel: true,
  host: 'localhost',
  port: 9000,
  logPrefix: "set name builder"
};

task('html:build', function () {
  return src(path.src.html)
    .pipe(rigger())
    .pipe(dest(path.build.html))
    .pipe(browserSync.stream({ stream: true }));
});

task('pug:build', function () {
  return src(path.src.pug)
    .pipe(pug({ pretty: true }))
    .pipe(dest(path.build.html))
    .pipe(browserSync.stream({ stream: true }));
});

task('js:build', function () {
  return src(path.src.js.common)
    .pipe(plumber({
      errorHandler: onError(function (err) {
        return {
          title: 'Scripts',
          message: err.message
        };
      })
    }))
    .pipe(gulpIf(isDevelopment, sourcemaps.init()))
    .pipe(babel({
      presets: ['@babel/preset-env']
    }))
    .pipe(gulpIf(!isDevelopment, uglify()))
    .pipe(concat('bundle.js'))
    .pipe(gulpIf(isDevelopment, sourcemaps.write()))
    .pipe(dest(path.build.js))
    .pipe(browserSync.stream({ stream: true }));
});

task('js-libs:build', function () {
  return src(path.src.js.libs)
    .pipe(newer(path.build.js_libs))
    .pipe(gulpIf(isDevelopment, sourcemaps.init()))
    .pipe(gulpIf(!isDevelopment, uglify()))
    .pipe(gulpIf(isDevelopment, sourcemaps.write()))
    .pipe(dest(path.build.js_libs))
    .pipe(browserSync.stream({ stream: true }));

});

task('style:build', function () {
  return src(path.src.style.main)
    .pipe(plumber({
      errorHandler: onError(function (err) {
        return {
          title: 'Style',
          message: err.message
        };
      })
    }))
    .pipe(gulpStylelint({
      reporters: [
        { formatter: 'string', console: true }
      ]
    }))
    .pipe(gulpIf(isDevelopment, sourcemaps.init()))
    .pipe(sass({
      sourceMap: true,
      errLogToConsole: true,
      includePaths: ['node_modules/bootstrap/dist/css', path.src.style.main]
    }))
    .pipe(autoprefixer({ browsers: ['last 2 versions'] }))
    .pipe(gulpIf(!isDevelopment, cssnano()))
    .pipe(gulpIf(isDevelopment, sourcemaps.write()))
    .pipe(dest(path.build.css))
    .pipe(browserSync.stream({ stream: true }));
});

task('stylelint', function () {
  return src(path.src.style.main)
    .pipe(plumber({
      errorHandler: onError(function (err) {
        return {
          title: 'Style',
          message: err.message
        };
      })
    }))
    .pipe(gulpStylelint({
      reporters: [
        { formatter: 'string', console: true }
      ]
    }))
});

// task('style-libs:build', function () {
//   return src(path.src.style.libs)
//       .pipe(newer(path.build.css_libs))
//       .pipe(cssnano())
//       .pipe(dest(path.build.css_libs))
//       .pipe(browserSync.stream({stream: true}));
// });

task('image', function () {
  return src(path.src.assets.img)
    .pipe(newer(path.build.img))
    .pipe(imagemin({
      progressive: true,
      use: [pngquant()],
      interlaced: true
    }))
    .pipe(dest(path.build.img))
    .pipe(browserSync.stream({ stream: true }));
});
task('svg', function () {
  return src(path.src.assets.svg)
    .pipe(newer(path.build.img))
    .pipe(dest(path.build.img))
    .pipe(browserSync.stream({ stream: true }));
});
task('images:build', parallel('image', 'svg'));

task('fonts:build', function () {
  return src(path.src.assets.fonts)
    .pipe(newer(path.build.fonts))
    .pipe(dest(path.build.fonts))
    .pipe(browserSync.stream({ stream: true }));
});

task('clean', function () {
  return del(path.clean);
});

task('watch', function () {
  const watchCallback = function (done) {
    browserSync.reload();
    done();
  };
  watch(path.watch.html, series('html:build'), watchCallback);
  watch(path.watch.pug, series('pug:build'), watchCallback);
  watch(path.watch.style, series('style:build'), watchCallback);
  // watch(path.watch.style, series('style-libs:build'), watchCallback);
  watch(path.watch.js, series('js:build'), watchCallback);
  // watch(path.watch.js, series('js-libs:build  '), watchCallback);
  watch(path.watch.img, series('images:build'), watchCallback);
  watch(path.watch.fonts, series('fonts:build'), watchCallback);
});

task('build',
  series('clean',
    parallel('html:build', 'pug:build', 'js-libs:build', 'js:build', 'style-libs:build', 'style:build', 'fonts:build', 'images:build')));

task('serve', function () {
  browserSync.init(config);
});

task('default', series('build', parallel('watch', 'serve')));
