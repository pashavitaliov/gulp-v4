$( document ).ready(() => {

  //Menu
  const handlerMenu = () => {
    let isOpen = false;
    const menu = $('#menu');
    const menuBtn = $('#menu-btn');
    const menuMask = $('#menu-mask');
    const body = $('body');

    const menuFadeIn = () => {
      menuMask.addClass('fade-enter open');

      setTimeout(() => {
        menuMask.removeClass('fade-enter')
      }, 300);
    };

    const menuFadeOut = () => {
      isOpen ? menuMask.addClass('fade-leave') : null;

      setTimeout(() => {
        menuMask.removeClass('fade-leave open')
      }, 300);
    };

    const blockBodyScroll = () => {
      body.css('overflow', 'hidden')
    };

    const unblockBodyScroll = () => {
      body.css('overflow', 'auto')
    };

    const menuToggle = () => {
      menuBtn.toggleClass('open');
      menu.toggleClass('open');

      // isOpen ? unblockBodyScroll() : blockBodyScroll();
      isOpen ? menuFadeOut() : menuFadeIn();

      isOpen = !isOpen;
    };

    const closeMenuClickMask = (e) => {
        menuBtn.removeClass('open');
        menu.removeClass('open');
        menuFadeOut();
        unblockBodyScroll();
        isOpen = !isOpen;
    };

    function navToAnchor(e) {
      e.preventDefault();
      menuFadeOut();
      menuToggle();

      let id  = $(this).attr('href');
      let topScroll = $(id).offset().top + 1;
      console.log(topScroll);
      $('body,html').animate({scrollTop: topScroll}, 500);
    }

    menuBtn.on('click', menuToggle);
    menuMask.on('click', closeMenuClickMask);
    menu.on("click","a", navToAnchor);
  };

  handlerMenu();



  //Lang
  const openLangList = () => {
    $('#lang-list > li').slideToggle();
  };

  $('#langMenuButton').on('click', openLangList);

  //Form
  const serializeContacForm = (e) => {
    e.preventDefault();

    let $data = {};
    const contactFormFieelds = $('#contact-form').find('input, textearea, select');

    contactFormFieelds.each(() => {
      if (this.type === 'radio' && this.checked) {
        $data[this.name] = $(this).val();
      }

      if (this.type !== 'radio') {
        $data[this.name] = $(this).val();
      }
    });
    // return $data;
  };

  $( "#contact-form" ).on( "submit", serializeContacForm);


  let menu_selector = ".menu-list";
  function onScroll() {
      let scroll_top = $(document).scrollTop();
      $(menu_selector + " a").each(function() {
          let hash = $(this).attr("href");
          let target = $(hash);

          if (target.position().top - 150 <= scroll_top && target.position().top + target.outerHeight() > scroll_top) {
              $(menu_selector + " a.active").removeClass("active");
              $(this).addClass("active");
          } else {
              $(this).removeClass("active");
          }
      });

  }

  $(document).on("scroll", onScroll);

});